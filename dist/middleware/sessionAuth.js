"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const express_session_1 = __importDefault(require("express-session"));
const register = (app) => {
    // Configure Express to use authentication sessions
    app.use(express_session_1.default({
        resave: true,
        saveUninitialized: false,
        secret: process.env.SESSION_SECRET
    }));
};
exports.register = register;
//# sourceMappingURL=sessionAuth.js.map