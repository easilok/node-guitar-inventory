"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const api = __importStar(require("./api"));
const userContext = {
    userinfo: {
        sub: '00abc12defg3hij4k5l6',
        name: 'First Last',
        locale: 'en-US',
        preferred_username: 'account@company.com',
        given_name: 'First',
        family_name: 'Last',
        zoneinfo: 'America/Los_Angeles',
        updated_at: 1539283620
    },
};
const register = (app) => {
    // define a route handler for the default home page
    app.get("/", (req, res) => {
        const user = userContext ? userContext.userinfo : null;
        const isAuthenticated = true;
        res.render("index", { isAuthenticated, user });
    });
    // define a secure route handler for the login page that redirects to /guitars
    app.get("/login", (req, res) => {
        res.redirect("/guitars");
    });
    // define a route to handle logout
    app.get("/logout", (req, res) => {
        res.redirect("/");
    });
    // define a secure route handler for the guitars page
    app.get("/guitars", (req, res) => {
        const user = userContext ? userContext.userinfo : null;
        const isAuthenticated = true;
        res.render("guitars", { isAuthenticated, user });
    });
    api.register(app);
};
exports.register = register;
//# sourceMappingURL=index.js.map