import * as express from "express";
import * as api from "./api";

const userContext = {
    userinfo: {
        sub: '00abc12defg3hij4k5l6',
        name: 'First Last',
        locale: 'en-US',
        preferred_username: 'account@company.com',
        given_name: 'First',
        family_name: 'Last',
        zoneinfo: 'America/Los_Angeles',
        updated_at: 1539283620
    },
}

export const register = ( app: express.Application ) => {

    // define a route handler for the default home page
    app.get( "/", ( req: any, res ) => {
        const user = userContext ? userContext.userinfo : null;
        const isAuthenticated = true;
        res.render( "index", { isAuthenticated, user } );
    } );

    // define a secure route handler for the login page that redirects to /guitars
    app.get( "/login", ( req, res ) => {
        res.redirect( "/guitars" );
    } );

    // define a route to handle logout
    app.get( "/logout", ( req: any, res ) => {
        res.redirect( "/" );
    } );

    // define a secure route handler for the guitars page
    app.get( "/guitars", ( req: any, res ) => {
        const user = userContext ? userContext.userinfo : null;
        const isAuthenticated = true;
        res.render( "guitars", { isAuthenticated, user } );
    } );

    api.register( app );
};
